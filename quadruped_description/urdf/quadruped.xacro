<?xml version="1.0"?>
<!-- Revolute-Revolute Manipulator -->
<robot name="quadruped" xmlns:xacro="http://www.ros.org/wiki/xacro">

  <!-- Constants for robot dimensions -->
  <xacro:property name="PI" value="3.1415926535897931"/>
  <xacro:property name="camera_link" value="0.05" /> <!-- Size of square 'camera' box -->
  <xacro:property name="axel_offset" value="0.05" /> <!-- Space btw top of beam and the each joint -->

  <xacro:property name="body_width" value="0.4"/>
  <xacro:property name="body_length" value="1.6"/>
  <xacro:property name="body_height" value="0.2"/>
  <xacro:property name="body_mass" value="5"/>
  <xacro:property name="body_link_z" value="2"/>


  <xacro:property name="leg_link0_width" value="0.16"/>
  <xacro:property name="leg_link0_length" value="0.16"/>
  <xacro:property name="leg_link0_height" value="0.16"/>
  <xacro:property name="leg_link0_mass" value="1"/>


  <xacro:property name="leg_link1_width" value="0.10"/>
  <xacro:property name="leg_link1_length" value="0.10"/>
  <xacro:property name="leg_link1_height" value="0.60"/>
  <xacro:property name="leg_link1_mass" value="1"/>

  <xacro:property name="joint2_radius" value="0.05"/>

  <xacro:property name="leg_link2_width" value="0.10"/>
  <xacro:property name="leg_link2_length" value="0.10"/>
  <xacro:property name="leg_link2_height" value="0.60"/>
  <xacro:property name="leg_link2_mass" value="1"/>


  <xacro:property name="leg_foot_width" value="0.16"/>
  <xacro:property name="leg_foot_length" value="0.16"/>
  <xacro:property name="leg_foot_height" value="0.16"/>
  <xacro:property name="leg_foot_mass" value="0.2"/>


  <!-- Import all Gazebo-customization elements, including Gazebo colors -->
  <xacro:include filename="$(find quadruped_description)/urdf/quadruped.gazebo" />
  <!-- Import Rviz colors -->
  <xacro:include filename="$(find quadruped_description)/urdf/materials.xacro" />

  <!-- Used for fixing robot to Gazebo 'base_link' -->
<!--
  <link name="world"/>

  <joint name="fixed_dummy" type="fixed">
    <parent link="world"/>
    <child link="body_link"/>
  </joint>
-->

  <!-- Body Link -->
  <link name="body_link">
    <collision>
      <origin xyz="0 0 ${body_link_z}" rpy="0 0 0"/>
      <geometry>
        <box size="${body_width} ${body_length} ${body_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 ${body_link_z}" rpy="0 0 0"/>
      <geometry>
        <box size="${body_width} ${body_length} ${body_height}"/>
      </geometry>
      <material name="grey"/>
    </visual>

    <inertial>
      <origin xyz="0 0 ${body_link_z}" rpy="0 0 0"/>
      <mass value="${body_mass}"/>
      <inertia
          ixx="${body_mass / 12.0 * (body_width*body_width + body_height*body_height)}" ixy="0.0" ixz="0.0"
          iyy="${body_mass / 12.0 * (body_height*body_height + body_width*body_width)}" iyz="0.0"
          izz="${body_mass / 12.0 * (body_width*body_width + body_width*body_width)}"/>
    </inertial>
  </link> <!-- End of Body Link -->


  <!-- =========== FrontLeft Leg ============ -->
  <!-- FrontLeft Leg Joint 0 -->
  <joint name="frontleft_leg_joint0" type="continuous">
    <parent link="body_link"/>
    <child link="frontleft_leg_link0"/>
    <origin xyz="${-1*body_width/2 - leg_link0_width/2} ${body_length/2 - leg_link0_length/2} ${body_link_z}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="0.7"/>
  </joint>


  <!-- FrontLeft Leg Link 0 -->
  <link name="frontleft_leg_link0">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
	<box size="${leg_link0_width} ${leg_link0_width} ${leg_link0_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
      <geometry>
        <cylinder radius="${leg_link0_width/2}" length="${leg_link0_length}"/> 
<!--        <box size="${leg_link0_width} ${leg_link0_length} ${leg_link0_height}"/> -->
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_link0_mass}"/>
      <inertia
	  ixx="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_height*leg_link0_height)}" ixy="0.0" ixz="0.0"
	  iyy="${leg_link0_mass / 12.0 * (leg_link0_height*leg_link0_height + leg_link0_width*leg_link0_width)}" iyz="0.0"
	  izz="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_width*leg_link0_width)}"/>
    </inertial>
  </link>
  
  <!-- FrontLeft Leg Joint 1 -->
  <joint name="frontleft_leg_joint1" type="continuous">
    <parent link="frontleft_leg_link0"/>
    <child link="frontleft_leg_link1"/>
    <origin xyz="${-1*leg_link0_width/2} 0 0" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>
 
  <!-- FrontLeft Leg Link 1 -->
  <link name="frontleft_leg_link1">
   <collision>
      <origin xyz="${-1*leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${-1*leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>

      <material name="black"/>
    </visual>

    <inertial>
      <origin xyz="${-1*leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link1_mass}"/>
      <inertia
          ixx="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_height*leg_link1_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link1_mass / 12.0 * (leg_link1_height*leg_link1_height + leg_link1_width*leg_link1_width)}" iyz="0.0"
          izz="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_width*leg_link1_width)}"/>
    </inertial>
  </link>

  <!-- FrontLeft Leg Joint 2 -->
  <joint name="frontleft_leg_joint2" type="continuous">
    <parent link="frontleft_leg_link1"/>
    <child link="frontleft_leg_link2"/>
    <origin xyz="0 0 ${ -1*leg_link1_height + leg_link0_height/2 + joint2_radius}" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>

  <!-- Frontleft Leg Link 2 -->
  <link name="frontleft_leg_link2">
   <collision>
      <origin xyz="${leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="${leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link2_mass}"/>
      <inertia
          ixx="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_height*leg_link2_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link2_mass / 12.0 * (leg_link2_height*leg_link2_height + leg_link2_width*leg_link2_width)}" iyz="0.0"
          izz="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_width*leg_link2_width)}"/>
    </inertial>
  </link>

  <!-- FrontLeft Leg Joint 3 / fixed -->
  <joint name="frontleft_leg_joint3_fixed" type="fixed">
    <parent link="frontleft_leg_link2"/>
    <child link="frontleft_leg_foot"/>
    <origin xyz="${leg_link2_width/2} 0 ${-1*leg_link2_height}" rpy="0 0 0"/>
  </joint>


  <!-- Frontleft Leg Foot -->
  <link name="frontleft_leg_foot">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
      <material name="red"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_foot_mass}"/>
      <inertia
          ixx="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_height*leg_foot_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_foot_mass / 12.0 * (leg_foot_height*leg_foot_height + leg_foot_width*leg_foot_width)}" iyz="0.0"
          izz="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_width*leg_foot_width)}"/>
    </inertial>
  </link>


  <transmission name="frontleft_leg_tran0">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="frontleft_leg_joint0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="frontleft_leg_tran1">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="frontleft_leg_joint1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="frontleft_leg_tran2">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="frontleft_leg_joint2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  <!-- =========== End of FrontLeft Leg ============ -->

  <!-- =========== FrontRight Leg ============ -->
  <!-- FrontRight Leg Joint 0 -->
  <joint name="frontright_leg_joint0" type="continuous">
    <parent link="body_link"/>
    <child link="frontright_leg_link0"/>
    <origin xyz="${body_width/2 + leg_link0_width/2} ${body_length/2 - leg_link0_length/2} ${body_link_z}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="0.7"/>
  </joint>


  <!-- FrontRight Leg Link 0 -->
  <link name="frontright_leg_link0">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
	<box size="${leg_link0_width} ${leg_link0_width} ${leg_link0_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
      <geometry>
        <cylinder radius="${leg_link0_width/2}" length="${leg_link0_length}"/> 
<!--        <box size="${leg_link0_width} ${leg_link0_length} ${leg_link0_height}"/> -->
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_link0_mass}"/>
      <inertia
	  ixx="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_height*leg_link0_height)}" ixy="0.0" ixz="0.0"
	  iyy="${leg_link0_mass / 12.0 * (leg_link0_height*leg_link0_height + leg_link0_width*leg_link0_width)}" iyz="0.0"
	  izz="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_width*leg_link0_width)}"/>
    </inertial>
  </link>
  
  <!-- FrontRight Leg Joint 1 -->
  <joint name="frontright_leg_joint1" type="continuous">
    <parent link="frontright_leg_link0"/>
    <child link="frontright_leg_link1"/>
    <origin xyz="${leg_link0_width/2} 0 0" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>
 
  <!-- FrontRight Leg Link 1 -->
  <link name="frontright_leg_link1">
   <collision>
      <origin xyz="${leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>

      <material name="black"/>
    </visual>

    <inertial>
      <origin xyz="${leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link1_mass}"/>
      <inertia
          ixx="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_height*leg_link1_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link1_mass / 12.0 * (leg_link1_height*leg_link1_height + leg_link1_width*leg_link1_width)}" iyz="0.0"
          izz="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_width*leg_link1_width)}"/>
    </inertial>
  </link>

  <!-- FrontRight Leg Joint 2 -->
  <joint name="frontright_leg_joint2" type="continuous">
    <parent link="frontright_leg_link1"/>
    <child link="frontright_leg_link2"/>
    <origin xyz="0 0 ${ -1*leg_link1_height + leg_link0_height/2 + joint2_radius}" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>

  <!-- FrontRight Leg Link 2 -->
  <link name="frontright_leg_link2">
   <collision>
      <origin xyz="${-1*leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${-1*leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="${-1*leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link2_mass}"/>
      <inertia
          ixx="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_height*leg_link2_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link2_mass / 12.0 * (leg_link2_height*leg_link2_height + leg_link2_width*leg_link2_width)}" iyz="0.0"
          izz="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_width*leg_link2_width)}"/>
    </inertial>
  </link>

  <!-- FrontRight Leg Joint 3 / fixed -->
  <joint name="frontright_leg_joint3_fixed" type="fixed">
    <parent link="frontright_leg_link2"/>
    <child link="frontright_leg_foot"/>
    <origin xyz="${-1*leg_link2_width/2} 0 ${-1*leg_link2_height}" rpy="0 0 0"/>
  </joint>


  <!-- FrontRightt Leg Foot -->
  <link name="frontright_leg_foot">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
      <material name="red"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_foot_mass}"/>
      <inertia
          ixx="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_height*leg_foot_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_foot_mass / 12.0 * (leg_foot_height*leg_foot_height + leg_foot_width*leg_foot_width)}" iyz="0.0"
          izz="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_width*leg_foot_width)}"/>
    </inertial>
  </link>


  <transmission name="frontright_leg_tran0">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="frontright_leg_joint0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="frontright_leg_tran1">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="frontright_leg_joint1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="frontright_leg_tran2">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="frontright_leg_joint2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  <!-- =========== End of FrontRight Leg ============ -->


  <!-- =========== RearLeft Leg ============ -->
  <!-- RearLeft Leg Joint 0 -->
  <joint name="rearleft_leg_joint0" type="continuous">
    <parent link="body_link"/>
    <child link="rearleft_leg_link0"/>
    <origin xyz="${-1*body_width/2 - leg_link0_width/2} ${-1*body_length/2 + leg_link0_length/2} ${body_link_z}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="0.7"/>
  </joint>


  <!-- RearLeft Leg Link 0 -->
  <link name="rearleft_leg_link0">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
	<box size="${leg_link0_width} ${leg_link0_width} ${leg_link0_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
      <geometry>
        <cylinder radius="${leg_link0_width/2}" length="${leg_link0_length}"/> 
<!--        <box size="${leg_link0_width} ${leg_link0_length} ${leg_link0_height}"/> -->
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_link0_mass}"/>
      <inertia
	  ixx="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_height*leg_link0_height)}" ixy="0.0" ixz="0.0"
	  iyy="${leg_link0_mass / 12.0 * (leg_link0_height*leg_link0_height + leg_link0_width*leg_link0_width)}" iyz="0.0"
	  izz="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_width*leg_link0_width)}"/>
    </inertial>
  </link>
  
  <!-- RearLeft Leg Joint 1 -->
  <joint name="rearleft_leg_joint1" type="continuous">
    <parent link="rearleft_leg_link0"/>
    <child link="rearleft_leg_link1"/>
    <origin xyz="${-1*leg_link0_width/2} 0 0" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>
 
  <!-- RearLeft Leg Link 1 -->
  <link name="rearleft_leg_link1">
   <collision>
      <origin xyz="${-1*leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${-1*leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>

      <material name="black"/>
    </visual>

    <inertial>
      <origin xyz="${-1*leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link1_mass}"/>
      <inertia
          ixx="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_height*leg_link1_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link1_mass / 12.0 * (leg_link1_height*leg_link1_height + leg_link1_width*leg_link1_width)}" iyz="0.0"
          izz="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_width*leg_link1_width)}"/>
    </inertial>
  </link>

  <!-- RearLeft Leg Joint 2 -->
  <joint name="rearleft_leg_joint2" type="continuous">
    <parent link="rearleft_leg_link1"/>
    <child link="rearleft_leg_link2"/>
    <origin xyz="0 0 ${ -1*leg_link1_height + leg_link0_height/2 + joint2_radius}" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>

  <!-- Rearleft Leg Link 2 -->
  <link name="rearleft_leg_link2">
   <collision>
      <origin xyz="${leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="${leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link2_mass}"/>
      <inertia
          ixx="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_height*leg_link2_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link2_mass / 12.0 * (leg_link2_height*leg_link2_height + leg_link2_width*leg_link2_width)}" iyz="0.0"
          izz="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_width*leg_link2_width)}"/>
    </inertial>
  </link>

  <!-- RearLeft Leg Joint 3 / fixed -->
  <joint name="rearleft_leg_joint3_fixed" type="fixed">
    <parent link="rearleft_leg_link2"/>
    <child link="rearleft_leg_foot"/>
    <origin xyz="${leg_link2_width/2} 0 ${-1*leg_link2_height}" rpy="0 0 0"/>
  </joint>


  <!-- RearLeft Leg Foot -->
  <link name="rearleft_leg_foot">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
      <material name="red"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_foot_mass}"/>
      <inertia
          ixx="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_height*leg_foot_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_foot_mass / 12.0 * (leg_foot_height*leg_foot_height + leg_foot_width*leg_foot_width)}" iyz="0.0"
          izz="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_width*leg_foot_width)}"/>
    </inertial>
  </link>


  <transmission name="rearleft_leg_tran0">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="rearleft_leg_joint0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="rearleft_leg_tran1">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="rearleft_leg_joint1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="rearleft_leg_tran2">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="rearleft_leg_joint2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  <!-- =========== End of RearLeft Leg ============ -->

  <!-- =========== RearRight Leg ============ -->
  <!-- RearRight Leg Joint 0 -->
  <joint name="rearright_leg_joint0" type="continuous">
    <parent link="body_link"/>
    <child link="rearright_leg_link0"/>
    <origin xyz="${body_width/2 + leg_link0_width/2} ${-1*body_length/2 + leg_link0_length/2} ${body_link_z}" rpy="0 0 0"/>
    <axis xyz="0 1 0"/>
    <dynamics damping="0.7"/>
  </joint>


  <!-- RearRight Leg Link 0 -->
  <link name="rearright_leg_link0">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
	<box size="${leg_link0_width} ${leg_link0_width} ${leg_link0_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
      <geometry>
        <cylinder radius="${leg_link0_width/2}" length="${leg_link0_length}"/> 
<!--        <box size="${leg_link0_width} ${leg_link0_length} ${leg_link0_height}"/> -->
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_link0_mass}"/>
      <inertia
	  ixx="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_height*leg_link0_height)}" ixy="0.0" ixz="0.0"
	  iyy="${leg_link0_mass / 12.0 * (leg_link0_height*leg_link0_height + leg_link0_width*leg_link0_width)}" iyz="0.0"
	  izz="${leg_link0_mass / 12.0 * (leg_link0_width*leg_link0_width + leg_link0_width*leg_link0_width)}"/>
    </inertial>
  </link>
  
  <!-- RearRight Leg Joint 1 -->
  <joint name="rearright_leg_joint1" type="continuous">
    <parent link="rearright_leg_link0"/>
    <child link="rearright_leg_link1"/>
    <origin xyz="${leg_link0_width/2} 0 0" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>
 
  <!-- RearRight Leg Link 1 -->
  <link name="rearright_leg_link1">
   <collision>
      <origin xyz="${leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link1_width} ${leg_link1_width} ${leg_link1_height}"/>
      </geometry>

      <material name="black"/>
    </visual>

    <inertial>
      <origin xyz="${leg_link1_width/2} 0 ${ -1*leg_link1_height/2 + leg_link0_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link1_mass}"/>
      <inertia
          ixx="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_height*leg_link1_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link1_mass / 12.0 * (leg_link1_height*leg_link1_height + leg_link1_width*leg_link1_width)}" iyz="0.0"
          izz="${leg_link1_mass / 12.0 * (leg_link1_width*leg_link1_width + leg_link1_width*leg_link1_width)}"/>
    </inertial>
  </link>

  <!-- RearRight Leg Joint 2 -->
  <joint name="rearright_leg_joint2" type="continuous">
    <parent link="rearright_leg_link1"/>
    <child link="rearright_leg_link2"/>
    <origin xyz="0 0 ${ -1*leg_link1_height + leg_link0_height/2 + joint2_radius}" rpy="0 0 0"/>
    <axis xyz="1 0 0"/>
    <dynamics damping="0.7"/>
  </joint>

  <!-- RearRight Leg Link 2 -->
  <link name="rearright_leg_link2">
   <collision>
      <origin xyz="${-1*leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="${-1*leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <geometry>
        <box size="${leg_link2_width} ${leg_link2_width} ${leg_link2_height}"/>
      </geometry>
      <material name="orange"/>
    </visual>

    <inertial>
      <origin xyz="${-1*leg_link2_width/2} 0 ${ joint2_radius - leg_link2_height/2}" rpy="0 0 0"/>
      <mass value="${leg_link2_mass}"/>
      <inertia
          ixx="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_height*leg_link2_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_link2_mass / 12.0 * (leg_link2_height*leg_link2_height + leg_link2_width*leg_link2_width)}" iyz="0.0"
          izz="${leg_link2_mass / 12.0 * (leg_link2_width*leg_link2_width + leg_link2_width*leg_link2_width)}"/>
    </inertial>
  </link>

  <!-- RearRight Leg Joint 3 / fixed -->
  <joint name="rearright_leg_joint3_fixed" type="fixed">
    <parent link="rearright_leg_link2"/>
    <child link="rearright_leg_foot"/>
    <origin xyz="${-1*leg_link2_width/2} 0 ${-1*leg_link2_height}" rpy="0 0 0"/>
  </joint>


  <!-- RearRightt Leg Foot -->
  <link name="rearright_leg_foot">
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
    </collision>

    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${leg_foot_width/2}"/>
      </geometry>
      <material name="red"/>
    </visual>

    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <mass value="${leg_foot_mass}"/>
      <inertia
          ixx="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_height*leg_foot_height)}" ixy="0.0" ixz="0.0"
          iyy="${leg_foot_mass / 12.0 * (leg_foot_height*leg_foot_height + leg_foot_width*leg_foot_width)}" iyz="0.0"
          izz="${leg_foot_mass / 12.0 * (leg_foot_width*leg_foot_width + leg_foot_width*leg_foot_width)}"/>
    </inertial>
  </link>


  <transmission name="rearright_leg_tran0">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="rearright_leg_joint0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor0">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="rearright_leg_tran1">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="rearright_leg_joint1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor1">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="rearright_leg_tran2">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="rearright_leg_joint2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor2">
      <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  <!-- =========== End of FrontRight Leg ============ -->



</robot>
