# Quadruped Gazebo ROS example 

This example was modified from the rrbot gazebo ros package originally created by Dave Coleman 
<davetcoleman@gmail.com>


## Quick Start
Step 0: Clone the example into your workspace

    change to your XXXX_ws/src
    git clone https://gitlab.com/dumbrobot00/quadruped.git

Step 1: Start Quadruped Gazebo:

    roslaunch quadruped_gazebo quadruped_world.launch

Step 2: Start ROS Control:

    roslaunch quadruped_control quadruped_control.launch

Step 3: Run the joint command publisher

    rosrun quadruped_test quadruped_test

