#include "ros/ros.h"
#include "std_msgs/Float64.h"

#include <sstream>

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "quadruped_test");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
  ros::Publisher frontleft_leg_j0_pub = n.advertise<std_msgs::Float64>("/quadruped/frontleft_leg_joint0_position_controller/command", 1000);
  ros::Publisher frontleft_leg_j1_pub = n.advertise<std_msgs::Float64>("/quadruped/frontleft_leg_joint1_position_controller/command", 1000);
  ros::Publisher frontleft_leg_j2_pub = n.advertise<std_msgs::Float64>("/quadruped/frontleft_leg_joint2_position_controller/command", 1000);
 
  ros::Rate loop_rate(20);

  /**
   * A count of how many messages we have sent. This is used to create
   * a unique string for each message.
   */
  int count = 0;
  int state = 0;

  /**
  * This is a message object. You stuff it with data, and then publish it.
  */
  std_msgs::Float64 frontleft_leg_j0_cmd;
  std_msgs::Float64 frontleft_leg_j1_cmd;
  std_msgs::Float64 frontleft_leg_j2_cmd;

  frontleft_leg_j0_cmd.data = 0.0;
  frontleft_leg_j1_cmd.data = 0.0;
  frontleft_leg_j2_cmd.data = 0.0;


  while (ros::ok())
  {
    std::cout << "state=" << state << std::endl;
    std::cout << "j1.cmd.data=" << frontleft_leg_j1_cmd.data << std::endl;



    if (state == 0) {
      frontleft_leg_j0_cmd.data = 0;
      frontleft_leg_j1_cmd.data = 0;
      frontleft_leg_j2_cmd.data = 0;
    }
    else if (state== 1) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = 0.7; //frontleft_leg_j1_cmd.data + 0.1;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data + 0.1;
      //frontleft_leg_j2_cmd.data = -0.9; //frontleft_leg_j2_cmd.data - 0.1;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data - 0.1;
    }
    else if (state == 2) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = 0.9; //frontleft_leg_j1_cmd.data;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data + 0.1;
      //frontleft_leg_j2_cmd.data = -0.1; //frontleft_leg_j2_cmd.data + 0.1;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data + 0.25;

    }
    else if (state == 3) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = -0.6; //frontleft_leg_j1_cmd.data - 0.1;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data - 0.1;
      //frontleft_leg_j2_cmd.data = 0.0; //frontleft_leg_j2_cmd.data;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data;
    }
    else if (state == 4) {
      frontleft_leg_j0_cmd.data = 0;
      //frontleft_leg_j1_cmd.data = -0.6; //frontleft_leg_j1_cmd.data;
      frontleft_leg_j1_cmd.data = frontleft_leg_j1_cmd.data;
      //frontleft_leg_j2_cmd.data = -0.9; //frontleft_leg_j2_cmd.data - 0.1;
      frontleft_leg_j2_cmd.data = frontleft_leg_j2_cmd.data - 0.1;
    }
    else {
      frontleft_leg_j0_cmd.data = 0;
      frontleft_leg_j1_cmd.data = 0;
      frontleft_leg_j2_cmd.data = 0;
    }

    // state transition logic
    if (state == 0) {
      state = 1;
    }
    else if (state== 1) {
      if(frontleft_leg_j1_cmd.data > 0.5) {
        state = 2;
      }
    }
    else if (state == 2) {
      if((frontleft_leg_j1_cmd.data > 0.6) && 
         (frontleft_leg_j2_cmd.data > -0.3) )
        state = 3;
    }
    else if (state == 3) {
      if((frontleft_leg_j1_cmd.data < -0.4))
        state = 4;
    }
    else if (state == 4) {
      if((frontleft_leg_j1_cmd.data < -0.4) &&
         (frontleft_leg_j2_cmd.data < -0.4) )
        state = 1;
    }
    else {
      state = 0;
    }


//    ROS_INFO("%s", cmd.data.c_str());

    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
    frontleft_leg_j0_pub.publish(frontleft_leg_j0_cmd);
    frontleft_leg_j1_pub.publish(frontleft_leg_j1_cmd);
    frontleft_leg_j2_pub.publish(frontleft_leg_j2_cmd);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
